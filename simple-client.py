import socket


def register():
    username = input("> Username: ")
    password = input("> Password: ")
    age = str(int(input("> Age: ")))
    phone = input("> Phone number: ")

    return "REG###{}###{}###{}###{}".format(username, password, phone, age)
    

def add_languages():
    username = input("> Username: ")

    languages = set()

    print("Add laguages, 'exit' to stop.")
    language = input("> ")

    while not "exit" == language:
        languages.add(language)
        language = input("> ")

    return "LNG###{}###{}".format(username, "###".join(languages))


def add_interests():
    username = input("> Username: ")

    interests = set()

    print("Add interests, 'exit' to stop.")
    interest = input("> ")

    while not "exit" == interest:
        interests.add(interest)
        interest = input("> ")

    return "INT###{}###{}".format(username, "###".join(interests))


def login():
    username = input("> Username: ")
    password = input("> Password: ")

    return "LOG###{}###{}".format(username, password)


def get_match():
    username = input("> Username: ")

    return "MTC###{}".format(username)


def block():
    blocker = input("> Blocker: ")
    blocked = input("> Blocked: ")

    return "BLC###{}###{}".format(blocker, blocked)


def drive():
    username = input("> Username: ")
    state = input("> State (BEGINNING/END): ")

    while not state == "BEGINNING" and not state == "END":
        state = input("> State (BEGINNING/END): ")

    return "DRV###{}###{}".format(username, state)


def show_menu():
    print("0. Exit")
    print("1. Register a new user")
    print("2. Add languages")
    print("3. Add interests")
    print("4. Login")
    print("5. Get match")
    print("6. Block someone")
    print("7. Driving state")


func_list = [register, add_languages, add_interests, login, get_match, block, drive]


if '__main__' == __name__:
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    print("CONNECTING")

    # client.connect(('ec2-18-216-96-62.us-east-2.compute.amazonaws.com', 8246))
    # client.connect(('51.105.27.210', 8246))
    client.connect(('10.0.0.22', 8246))

    print("CONNECTED")
    print()

    show_menu()
    option = int(input("Your choice: "))
    message = str()

    while not 0 == option:
        message = func_list[option - 1]()

        client.send(bytes(message + '\n', 'utf-8'))
        print("+++", message)

        # Get the first byte of the message.
        response = client.recv(1).decode('utf-8')

        if len(response) == 0:
            break
            
        # Get the next bytes.
        while response[-1] != '\n':
            response += client.recv(1).decode('utf-8')

        print("---", response)
        print()

        show_menu()
        option = int(input("Your choice: "))
        
    client.close()

