# This file contains the classes of all the messages from the user.

import users_db
import blocked_db
import languages_db
import interests_db
import temporary_blocks_db

from time import strftime
TIME_FORMAT = "%Y/%m/%d-%H:%M:%S"


class BlockMessage:
    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.data = data[3:]  # The message
        self.sock = sock        # The socket between the client and the server
        self.code = code        # The message's code

        self.process()

    def process(self):
        """
        This function processes the message.
        The function creates the relevant variables for the message,
        based on its code.
        :return: None.
        """

        message_parts = self.data.split("###")

        self.username = message_parts[0]
        self.user_to_block = message_parts[1]

    def handle(self):
        response = "SUCCESS"

        users_db.create_table()
        blocked_db.create_table()

        if blocked_db.row_exists(self.username, self.user_to_block):
            response = "USER_ALREADY_BLOCKED"
        elif not blocked_db.insert_row(self.username, self.user_to_block):
            response = "FAILED"
        else:
            # In the app you can block only after getting a match
            users_db.set_match(self.username)
            users_db.set_match(self.user_to_block)

        self.sock.send(bytes(response + '\n', 'utf-8'))
        print(strftime(TIME_FORMAT), "\t", "+", response, "\t({} wanted to block {})".format(self.username, self.user_to_block))


class DrivingMessage:
    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.data = data[3:]    # The message
        self.sock = sock        # The socket between the client and the server
        self.code = code        # The message's code

        self.process()

    def process(self):
        """
        This function processes the message.
        The function creates the relevant variables for the message,
        based on its code.
        :return: None.
        """

        message_parts = self.data.split("###")

        self.username = message_parts[0]
        self.state = message_parts[1] # "BEGINNING" or "END"

    def handle(self):
        # response = "SUCCESS"

        users_db.create_table()

        if not users_db.user_exists(self.username):
            pass
        elif "BEGINNING" == self.state:
            users_db.set_driving(self.username, True)
            print(strftime(TIME_FORMAT), '\t(the user {} is driving)'.format(self.username))
        elif "END" == self.state:
            match_username = users_db.get_match(self.username)

            if not "None" == match_username:
                users_db.set_match(match_username)
                users_db.set_match(self.username)
            
            users_db.set_driving(self.username, False)
            print(strftime(TIME_FORMAT), '\t(the user {} is not driving)'.format(self.username))

        # self.sock.send(bytes(response + '\n', 'utf-8'))
        print(strftime(TIME_FORMAT), "\t(the user {} wanted to update their driving state".format(self.username))
        # print(strftime(TIME_FORMAT), "\t", "+", response, "\t(the user {} wanted to update their driving state".format(self.username))


class InterestsMessage:
    """ This class represents an interests message from the client. """

    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.data = data[3:]    # The message
        self.sock = sock        # The socket between the client and the server
        self.code = code        # The message's code

        self.process()

    def process(self):
        """
        This function processes the message.
        The function creates the relevant variables for the message,
        based on its code.
        :return: None.
        """

        message_parts = self.data.split("###")

        self.username = message_parts[0]
        self.interests = message_parts[1:]

    def handle(self):
        response = str()
        successes = 0

        users_db.create_table()
        interests_db.create_table()

        interests_db.delete_interests_of(self.username)

        for interest in self.interests:
            if interests_db.insert_row(interest, self.username):
                successes += 1

        if 0 == successes:
            response = "FAILED"
        else:
            response = "SUCCESS"

        response += "###" + str(successes) + "###" + str(len(self.interests))

        self.sock.send(bytes(response + '\n', 'utf-8'))

        print(strftime(TIME_FORMAT), "\t", "+", response, "\t(tried to add interests to the user {})".format(self.username))


class LanguagesMessage:
    """ This class represents a languages message from the client. """

    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.data = data[3:]  # The message
        self.sock = sock        # The socket between the client and the server
        self.code = code        # The message's code

        self.process()

    def process(self):
        """
        This function processes the message.
        The function creates the relevant variables for the message,
        based on its code.
        :return: None.
        """

        message_parts = self.data.split("###")

        self.username = message_parts[0]
        self.languages = message_parts[1:]

    def handle(self):
        response = str()        # The message to the client
        successes = 0           # How many languages were added?
        num_of_languages = 0    # How many languages to add?

        users_db.create_table()
        languages_db.create_table()

        for language in self.languages:
            if "None" != language:
                num_of_languages += 1

                if languages_db.insert_row(language, self.username):
                    successes += 1

        if 0 == successes:
            response = "FAILED"
        else:
            response = "SUCCESS"

        response += "###" + str(successes) + "###" + str(num_of_languages)

        self.sock.send(bytes(response + '\n', 'utf-8'))
        
        print(strftime(TIME_FORMAT), "\t", "+", response, "\t(tried to add languages to the user {})".format(self.username))


class LoginMessage:
    """ This class represents a login message from the client. """

    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.data = data[3:]  # The message
        self.sock = sock        # The socket between the client and the server
        self.code = code        # The message's code

        self.process()

    def process(self):
        """
        This function processes the message.
        The function creates the relevant variables for the message,
        based on its code.
        :return: None.
        """

        message_parts = self.data.split("###")

        self.username = message_parts[0]
        self.password = message_parts[1]

    def handle(self):
        response = "SUCCESS"

        users_db.create_table()

        if not users_db.user_exists(self.username):
            response = "WRONG_USERNAME"
        elif not users_db.is_correct(self.username, self.password):
            response = "WRONG_PASSWORD"
        elif users_db.is_online(self.username):
            response = "USER_ONLINE"
        else:
            users_db.set_online(self.username, True)

        self.sock.send(bytes(response + '\n', 'utf-8'))
        print(strftime(TIME_FORMAT), "\t", "+", response, "\t(The user {} tried to log in)".format(self.username))


class BusyMessage:
    """ This class represents a login message from the client. """

    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.data = data[3:]  # The message
        self.sock = sock        # The socket between the client and the server
        self.code = code        # The message's code

        self.process()

    def process(self):
        """
        This function processes the message.
        The function creates the relevant variables for the message,
        based on its code.
        :return: None.
        """

        message_parts = self.data.split("###")

        self.username = message_parts[0]
        self.is_busy = message_parts[1]

    def handle(self):
        users_db.create_table()

        if ("False" == self.is_busy):
            users_db.set_busy(self.username, False)
            print(strftime(TIME_FORMAT), "\t", "(The user {} is free)".format(self.username))
        if ("True" == self.is_busy):
            users_db.set_busy(self.username, True)
            print(strftime(TIME_FORMAT), "\t", "(The user {} is busy)".format(self.username))

        # self.sock.send(bytes(response + '\n', 'utf-8'))


class MatchMessage:
    """ This class represents a match message from the client. """

    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.username = data[3:]    # The username
        self.sock = sock            # The socket between the client and the server
        self.code = code            # The message's code

    def get_match_dictionary(self, other):
        """
        This function calculates the match-points between two users.\n
        In case of an error (one (or both) of the users does not exist),
        the function returns a new dictionary with points=-1 (because (0, ...) 
        is a legitimate score, even if both users exist).\n

        :param self: This user.\n
        :param other: The second user's username.
        """

        match_points = 0

        dictionary = {
            "points": int(), 
            "username": str(),
            "age": int(),
            "common_interests": set(),
            "common_languages": set()
        }

        # First check the age difference

        first_age = users_db.get_age(self.username)
        second_age = users_db.get_age(other)

        if -1 == first_age or -1 == second_age:
            return dict()

        age_difference = abs(first_age - second_age)

        # Find common interests

        common_interests = interests_db.get_common_interests(self.username, other)

        if len(common_interests) == 0:
            return dict()

        # Find common languages

        common_languages = languages_db.get_common_languages(self.username, other)

        if len(common_languages) == 0:
            return dict()

        # Update dictionary
        
        if age_difference <= 3:
            match_points += 3
        elif age_difference <= 8:
            match_points += 2
        elif age_difference <= 13:
            match_points += 1

        match_points += 3 * len(common_interests)

        match_points += 1 * len(common_languages)

        dictionary["points"] = match_points
        dictionary["username"] = other
        dictionary["age"] = second_age
        dictionary["common_interests"] = common_interests
        dictionary["common_languages"] = common_languages

        return dictionary

    def handle(self):
        blocked_db.create_table()
        users_db.create_table()
        temporary_blocks_db.create_table()

        # Get the current match
        match = users_db.get_match(self.username)

        # The user has a match
        if not "None" == match:
            phone_number = users_db.get_phone_number(match)
            age = users_db.get_age(match)
            common_interests = interests_db.get_common_interests(self.username, match)
            common_languages = languages_db.get_common_languages(self.username, match)
            is_active = users_db.is_active(self.username)

            response = "MATCH###{}###{}###{}###{}###{}###{}".format(
                match,                         
                phone_number,                                   
                age,                              
                '@@@'.join(common_interests),     
                '@@@'.join(common_languages),
                str(is_active)      
            )

            users_db.add_points(self.username, 10)
            self.sock.send(bytes(response + '\n', 'utf-8'))
            print(strftime(TIME_FORMAT), "\t", "+", response, "\t(the user {} asked for a match)".format(self.username))
            temporary_blocks_db.insert_row(self.username, match)
            
            users_db.set_match(self.username)
            users_db.set_match(match)
            users_db.set_active(self.username, False)
            users_db.set_active(match, False)

            return

        max_points = 0                  # The highest match-points
        best_match = dict()             # A dictionary that contains info about the match

        users = users_db.get_users_without_a_match()
        blocked_users = blocked_db.get_users_blocked_by(self.username)
        blocked_me = blocked_db.get_users_who_blocked(self.username)
        temporary_blocked_users = temporary_blocks_db.get_users_blocked_by(self.username)
        temporary_blocked_me = temporary_blocks_db.get_users_who_blocked(self.username)
        
        response = str()

        for user in users:
            if user == self.username or user in blocked_users or user in blocked_me or not users_db.is_driving(user):
                continue
            elif user in temporary_blocked_users or user in temporary_blocked_me or not users_db.is_online(user):
                continue
            elif "True" == users_db.is_busy(user):
                continue

            match_dictionary = self.get_match_dictionary(user)

            if not len(match_dictionary) == 0:
                points = match_dictionary["points"]

                if max_points < points:
                    max_points = points
                    best_match = match_dictionary 

        if len(best_match) == 0:
            response = "NO_MATCH"
        else:
            users_db.add_points(self.username, 10)
            match = best_match["username"]
            phone_number = users_db.get_phone_number(best_match["username"])

            response = "MATCH###{}###{}###{}###{}###{}###True".format(
                best_match["username"],                         
                phone_number,                                   
                best_match["age"],                              
                '@@@'.join(best_match["common_interests"]),     
                '@@@'.join(best_match["common_languages"])      
            )

            # users_db.set_match(self.username, best_match["username"])
            users_db.set_match(best_match["username"], self.username)
            users_db.set_active(self.username, True)
            users_db.set_active(best_match["username"], False)
            temporary_blocks_db.insert_row(self.username, match)

        self.sock.send(bytes(response + '\n', 'utf-8'))
        print(strftime(TIME_FORMAT), "\t", "+", response, "\t(the user {} asked for a match)".format(self.username))


class RegistrationMessage:
    """ This class represents a registraion message from the client. """

    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.data = data[3:]    # The message
        self.sock = sock        # The socket between the client and the server
        self.code = code        # The message's code

        self.process()

    def process(self):
        """
        This function processes the message.
        The function creates the relevant variables for the message,
        based on its code.
        :return: None.
        """

        message_parts = self.data.split("###")

        self.username = message_parts[0]
        self.password = message_parts[1]
        self.phone_number = message_parts[2]
        self.age = int(message_parts[3])

    def handle(self):
        response = str()

        users_db.create_table()

        if users_db.user_exists(self.username):
            response = "USER_EXISTS"
        elif users_db.insert_user(self.username, self.password, self.phone_number, self.age):
            response = "SUCCESS"
        else:
            response = "FAILED"

        self.sock.send(bytes(response + '\n', 'utf-8'))

        print(strftime(TIME_FORMAT), "\t", "+", response, "\t(tried to register the user {})".format(self.username))


class PointsMessage:
    """ This class represents a get-points message from the client. """

    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.username = data[3:]    # The message
        self.sock = sock            # The socket between the client and the server
        self.code = code            # The message's code
    
    def handle(self):
        response = str()

        users_db.create_table()

        if not users_db.user_exists(self.username):
            response = "USER_DO_NOT_EXIST"
        else:
            response = "POINTS###" + str(users_db.get_points(self.username))

        self.sock.send(bytes(response + '\n', 'utf-8'))
        print(strftime(TIME_FORMAT), "\t", "+", response, "\t({}'s points)".format(self.username))

