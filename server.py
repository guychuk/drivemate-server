import socket
from threading import Thread, Lock
import users_db
from messages import *

from time import strftime
TIME_FORMAT = "%Y/%m/%d-%H:%M:%S"

from_clients = []           # This list contains the messages that the server needs to handle.
sockets = set()             # This is a set that contains the sockets that talk to the server.

mutex = Lock()

def talk_to_client(client_socket):
    username = str()
    users_db.create_table()
    temporary_blocks_db.create_table()

    while True:
        message = None

        code = get_next_message_code(client_socket)

        if (len(code) == 0):
            break
        
        data = get_message_data(client_socket)

        if (len(data) == 0):
            break

        print(strftime(TIME_FORMAT), "\t", "-", code + data)

        if "REG" == code:
            message = RegistrationMessage(code, client_socket, data) 
        elif "LNG" == code:
            message = LanguagesMessage(code, client_socket, data)
        elif "INT" == code:
            message =  InterestsMessage(code, client_socket, data)
        elif "LOG" == code:
            message =  LoginMessage(code, client_socket, data)
            if users_db.is_correct(message.username, message.password):
                username = message.username  
        elif "MTC" == code:
            message = MatchMessage(code, client_socket, data)
        elif "BLC" == code:
            message = BlockMessage(code, client_socket, data)
        elif "DRV" == code:
            message = DrivingMessage(code, client_socket, data)
        elif "PNT" == code:
            message = PointsMessage(code, client_socket, data)
        elif "BSY" == code:
            message = BusyMessage(code, client_socket, data)
            
        if None != message:
            mutex.acquire()
            from_clients.append(message)
            mutex.release()

    if len(username) == 0:
        print(strftime(TIME_FORMAT), '\t', "* Unknown user disconnected")
    else:
        users_db.set_driving(username, False)
        print(strftime(TIME_FORMAT), '\t', "* The user {} is not driving.".format(username))
        users_db.set_match(users_db.get_match(username))
        users_db.set_match(username)
        users_db.set_online(username, False)
        temporary_blocks_db.reset_blocks_of(username)
        users_db.set_busy(username, False)
        print(strftime(TIME_FORMAT), '\t', "* The user {} disconnected".format(username))


def get_next_message_code(connection):
    try:
        return connection.recv(3).decode('utf-8')
    except Exception:
        return str()


def get_message_data(connection):
    data = str()

    try:
        next_char = connection.recv(1).decode('utf-8')
    except Exception:
        return str()
    
    try:
        while next_char != '\n':
            data += next_char
            next_char = connection.recv(1).decode('utf-8')
    except Exception:
        return str()

    return data


def handle_messages():
    while True:
        while len(from_clients) == 0:
            pass
        mutex.acquire()
        message_to_handle = from_clients.pop()
        mutex.release()

        message_to_handle.handle()


if __name__ == '__main__':
    users_db.create_table()
    temporary_blocks_db.create_table()

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server.bind(('0.0.0.0', 8246))
    # server.bind(('', 8246))
    print(strftime(TIME_FORMAT), "\t", "* Binded")

    server.listen()
    print(strftime(TIME_FORMAT), "\t", "* Listening")

    answers_thread = Thread(target=handle_messages)
    answers_thread.start()

    while True:
        try:
        # Create a socket.
            client_socket = server.accept()[0]
            sockets.add(client_socket)
            print(strftime(TIME_FORMAT), "\t", "* Client accepted")

            # Create a new thread for the client.
            client_thread = Thread(target=talk_to_client, args=(client_socket,))
            client_thread.start()
        except ConnectionResetError:
            print(strftime(TIME_FORMAT), "\t", "* Client disconnected")

    server.close()
