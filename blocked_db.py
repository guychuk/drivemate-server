import sqlite3

from time import strftime
TIME_FORMAT = "%Y/%m/%d-%H:%M:%S"


def table_exists():
    """
    Checks if a table exists in the db.
    :return: True if exists, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    answer = cursor.fetchall()

    connection.close()

    return ("blocks",) in answer


def create_table():
    """
    Creates the table in the db.
    :return: None.
    """

    if not table_exists():
        print(strftime(TIME_FORMAT), "\t", "* Creating blocks table")
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE blocks(blocker TEXT, blocked TEXT)")
        connection.commit()
        connection.close()

    return None


def row_exists(blocker, blocked):
    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM blocks WHERE blocker = ? AND blocked = ?", [blocker, blocked])
    answer = cursor.fetchall()

    connection.close()

    return not len(answer) == 0


def insert_row(blocker, blocked):
    # Arguments can't be empty
    if len(blocker) == 0 or len(blocked) == 0:
        return False

    # If the row exists, then this function does nothing
    if row_exists(blocker, blocked):
        return False

    result = bool()

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    print(strftime(TIME_FORMAT), "\t", "* Blocking the option to match {} and {})".format(blocked, blocker))
    cursor.execute("INSERT INTO blocks(blocker, blocked) VALUES(?, ?)", (blocker, blocked))
    connection.commit()

    connection.close()

    result = row_exists(blocker, blocked)

    if result:
        print(strftime(TIME_FORMAT), "\t", "* Blocked the option to match {} and {}".format(blocker, blocked))
    else:
        print(strftime(TIME_FORMAT), "\t", "* Failed to block the option to match {} and {}".format(blocker, blocked))

    return result


def get_users_blocked_by(username):
    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT blocked FROM blocks WHERE blocker = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return list()

    blocked = list()

    for user in answer:
        blocked.append(user[0])

    return blocked


def get_users_who_blocked(username):
    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT blocker FROM blocks WHERE blocked = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return list()

    blockers = list()

    for user in answer:
        blockers.append(user[0])

    return blockers


def print_table():
    """
    This function prints the table.
    :return: None.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM blocks")
    answer = cursor.fetchall()

    connection.close()

    print(strftime(TIME_FORMAT), "\t", "Blocks table:")

    for user in answer:
        print(user[0], '\t', user[1], '\t', user[2], '\t', user[3], '\t', user[4])

    print()

    return None
