import sqlite3
import users_db

from time import strftime
TIME_FORMAT = "%Y/%m/%d-%H:%M:%S"


def create_table():
    """
    Creates the table in the db.
    :return: None.
    """

    if not table_exists():
        print(strftime(TIME_FORMAT), "\t", "* Creating languages table")
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE languages(language TEXT, username TEXT)")
        connection.commit()
        connection.close()

    return None


def table_exists():
    """
    Checks if a table exists in the db.
    :return: True if exists, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    answer = cursor.fetchall()

    connection.close()

    return ("languages",) in answer


def user_exists(username):
    """
    Checks if a user exists in the table.
    :param username: The username of the user.
    :return: True of exists, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM languages WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    return bool(len(answer))


def row_exists(language, username):
    """
    Checks if a row exists in the table.
    :param language: A language.
    :param username: The username of the user.
    :return: True of exists, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM languages WHERE language = ? AND username = ?", [language, username])
    answer = cursor.fetchall()

    connection.close()

    return len(answer) != 0


def insert_row(language, username):
    """
    This function inserts a new user to the table.
    :param language: A language that the user speaks.
    :param username: The user's username.
    :return: True if inserted, False if not.
    """

    # Username and language can't be empty
    if len(username) == 0 or len(language) == 0:
        return False

    if users_db.user_exists(username) == False:
        return False

    # If the row exists, then this function does nothing
    if row_exists(language, username):
        return False

    result = bool()

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    print(strftime(TIME_FORMAT), "\t", "* Adding the language '{}' to the user {}".format(language, username))
    cursor.execute("INSERT INTO languages(language, username) VALUES(?, ?)", (language, username))
    connection.commit()

    connection.close()

    result = row_exists(language, username)

    if result:
        print(strftime(TIME_FORMAT), "\t", "* Added the language '{}' to the user {}".format(language, username))
    else:
        print(strftime(TIME_FORMAT), "\t", "* Failed to add the language '{}' to the user {}".format(language, username))

    return result


def get_languages(username):
    """
    Get a user's languages.
    :param username: The username of the user.
    :return: The age. Return an empty set if there's an error.
    """

    if not user_exists(username):
        return set()

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT language FROM languages WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return set()

    languages = set()

    for language in answer:
        languages.add(language[0])

    return languages


def get_common_languages(user1, user2):
    if len(user1) == 0 or len(user2) == 0 or not user_exists(user1) or not user_exists(user2):
        return set()

    first_languages = get_languages(user1)
    second_languages = get_languages(user2)

    if len(first_languages) == 0 or len(second_languages) == 0:
        return set()

    common_languages = first_languages.intersection(second_languages)
    return common_languages


def print_table():
    """
    This function prints the table.
    :return: None.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM language")
    answer = cursor.fetchall()

    connection.close()

    print(strftime(TIME_FORMAT), "\t", "Languages table:")

    for language in answer:
        print(language[0], language[1])

    print()

    return None
