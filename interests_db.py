import sqlite3
import users_db

from time import strftime
TIME_FORMAT = "%Y/%m/%d-%H:%M:%S"


def create_table():
    """
    Creates the table in the db.
    :return: None.
    """

    if not table_exists():
        print(strftime(TIME_FORMAT), "\t", "* Creating interests table")
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE interests(interest TEXT, username TEXT)")
        connection.commit()
        connection.close()

    return None


def table_exists():
    """
    Checks if a table exists in the db.
    :return: True if exists, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    answer = cursor.fetchall()

    connection.close()

    return ("interests",) in answer


def user_exists(username):
    """
    Checks if a user exists in the table.
    :param username: The username of the user.
    :return: True of exists, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM interests WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    return bool(len(answer))


def row_exists(interest, username):
    """
    Checks if a row exists in the table.
    :param interest: A user's interest.
    :param username: The username of the user.
    :return: True of exists, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM interests WHERE interest = ? AND username = ?", [interest, username])
    answer = cursor.fetchall()

    connection.close()

    return len(answer) != 0


def insert_row(interest, username):
    """
    This function inserts a new user to the table.
    :param language: A language that the user speaks.
    :param username: The user's username.
    :return: True if inserted, False if not.
    """

    # Username and language can't be empty
    if len(username) == 0 or len(interest) == 0:
        return False

    if users_db.user_exists(username) == False:
        return False

    # If the row exists, then this function does nothing
    if row_exists(interest, username):
        return False

    result = bool()

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    print(strftime(TIME_FORMAT), "\t", "* Adding the interest '{}' to the user {}".format(interest, username))
    cursor.execute("INSERT INTO interests(interest, username) VALUES(?, ?)", (interest, username))
    connection.commit()

    connection.close()

    result = row_exists(interest, username)

    if result:
        print(strftime(TIME_FORMAT), "\t", "* Added the interest '{}' to the user {}".format(interest, username))
    else:
        print(strftime(TIME_FORMAT), "\t", "* Failed to add the interest '{}' to the user {}".format(interest, username))

    return result


def delete_interests_of(username):
    if (len(username) == 0):
        return False

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("DELETE FROM interests WHERE username = ?", (username,))
    connection.commit()
    connection.close()

    print(strftime(TIME_FORMAT), "\t", "* Deleting the interests of the user {}".format(username))

    return True


def get_interests(username):
    """
    Get a user's interests.
    :param username: The username of the user.
    :return: The age. Return an empty set if there's an error.
    """

    if not user_exists(username):
        return set()

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT interest FROM interests WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return set()

    interests = set()

    for interest in answer:
        interests.add(interest[0])

    return interests


def get_common_interests(user1, user2):
    if len(user1) == 0 or len(user2) == 0 or not user_exists(user1) or not user_exists(user2):
        return set()

    first_interests_set = get_interests(user1)
    second_interests_set = get_interests(user2)

    if len(first_interests_set) == 0 or len(second_interests_set) == 0:
        return set()

    common_interests = first_interests_set.intersection(second_interests_set)
    
    return common_interests


def print_table():
    """
    This function prints the table.
    :return: None.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM interests")
    answer = cursor.fetchall()

    connection.close()

    print(strftime(TIME_FORMAT), "\t", "Interests table:")

    for interest in answer:
        print(interest[0], interest[1])

    print()

    return None

############# TESTS #############

