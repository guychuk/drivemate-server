import sqlite3
import interests_db
import languages_db

from time import strftime
TIME_FORMAT = "%Y/%m/%d-%H:%M:%S"

def table_exists():
    """
    Checks if a table exists in the db.
    :return: True if exists, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    answer = cursor.fetchall()

    connection.close()

    return ("users",) in answer


def create_table():
    """
    Creates the table in the db.
    :return: None.
    """

    if not table_exists():
        print(strftime(TIME_FORMAT), "\t", "* Creating users table")
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE users(username TEXT, password TEXT, phone TEXT, age INTEGER, match TEXT, driving TEXT, active TEXT, online TEXT, points INTEGER, busy TEXT)")
        connection.commit()
        connection.close()

    return None


def user_exists(username):
    """
    Checks if a user exists in the table.
    :param username: The username of the user.
    :return: True of exists, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    return bool(len(answer))


def insert_user(username, password, phone, age):
    """
    This function inserts a new user to the table.
    :param username: The user's username.
    :param password: The user's password.
    :param phone: The user's phone number.
    :param age: The user's age.
    :return: Trye if inserted, False if not.
    """

    # Username and password can't be empty
    if len(username) == 0 or len(password) == 0 or len(phone) == 0 or age == 0:
        return False

    # If the user exists, then this function does nothing
    if user_exists(username):
        return False

    result = bool()

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    print(strftime(TIME_FORMAT), "\t", "* Inserting the user", username)
    cursor.execute("INSERT INTO users(username, password, phone, age, match, driving, active, online, points, busy) VALUES(?, ?, ?, ?, 'None', 'False', 'False', 'False', 100, 'False')", 
                    (username, password, phone, age))    
    connection.commit()

    connection.close()

    result = user_exists(username)

    if result:
        print(strftime(TIME_FORMAT), "\t", "* Inserted the user {} to the users table".format(username))
    else:
        print(strftime(TIME_FORMAT), "\t", "* Failed to insert the user {} to the users table".format(username))

    return result


def is_correct(username, password):
    """
    Check if the password matches the username.
    :param username: A username.
    :param password: A password.
    :return: True if match, False if not.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM users WHERE username = ? AND password = ?", [username, password])
    answer = cursor.fetchall()

    connection.close()

    return bool(len(answer))


def get_age(username):
    """
    Get a user's age.
    :param username: The username of the user.
    :return: The age. -1 if there's an error.
    """

    if not user_exists(username):
        return -1

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT age FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return -1

    return answer[0][0]


def get_points(username):
    """
    :param username: The username of the user.
    :return: Points. -1 if there's an error.
    """

    if not user_exists(username):
        return -1

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT points FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return -1

    return answer[0][0]


def get_match(username):

    if not user_exists(username):
        return str()

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT match FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return str()

    return answer[0][0]


def is_busy(username):

    if not user_exists(username):
        return str()

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT busy FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return str()

    return answer[0][0]


def get_phone_number(username):
    """
    Get a user's phone number.
    :param username: The username of the user.
    :return: The age. Empty string if there's an error.
    """

    if not user_exists(username):
        return str()

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT phone FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return str()

    return answer[0][0]


def is_driving(username):
    if len(username) == 0 or not user_exists(username):
        return False

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT driving FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return False

    if 'True' == answer[0][0]:
        return True

    return False


def get_all_users():
    """
    Get the users in the table.
    :return: A list of users.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT username FROM users")
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return list()

    usernames = list()

    for username in answer:
        usernames.append(username[0])

    return usernames


def get_users_without_a_match():
    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT username FROM users WHERE match = 'None'")
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return list()

    usernames = list()

    for username in answer:
        usernames.append(username[0])

    return usernames


def set_match(username, match = 'None'):
    if type(match) is not str:
        return False

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("UPDATE users SET match = ? WHERE username = ?", (match, username))

    connection.commit()
    connection.close()

    if "None" == match:
        set_active(username, False)

    return True


def set_busy(username, busy = 'True'):
    if type(busy) is not bool:
        return False

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("UPDATE users SET busy = ? WHERE username = ?", (str(busy), username))

    connection.commit()
    connection.close()

    return True


def add_points(username, to_add):
    """
    :param username: The username of the user.
    :return: The age. -1 if there's an error.
    """

    if not user_exists(username):
        return False

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("UPDATE users SET points = points + ? WHERE username = ?", (to_add, username))
    connection.commit()
    connection.close()

    return True


def set_driving(username, driving = True):
    if type(driving) is not bool or len(username) == 0 or not user_exists(username):
        return False
    
    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("UPDATE users SET driving = ? WHERE username = ?", (str(driving), username))

    connection.commit()
    connection.close()

    return True


def is_active(username):
    if not user_exists(username):
        return False

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT active FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    if (len(answer) == 0):
        return str()

    return "True" == answer[0][0]


def set_active(username, active = True):
    if type(active) is not bool or len(username) == 0 or not user_exists(username):
        return False
    
    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("UPDATE users SET active = ? WHERE username = ?", (str(active), username))

    connection.commit()
    connection.close()

    return True


def set_online(username, online = True):
    if type(online) is not bool or len(username) == 0 or not user_exists(username):
        return False
    
    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("UPDATE users SET online = ? WHERE username = ?", (str(online), username))

    connection.commit()
    connection.close()

    return True


def is_online(username):
    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT online FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    return 'True' == answer[0][0]


def print_table():
    """
    This function prints the table.
    :return: None.
    """

    connection = sqlite3.connect("data.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM users")
    answer = cursor.fetchall()

    connection.close()

    print(strftime(TIME_FORMAT), "\t", "Users table:")

    for user in answer:
        print(user[0], '\t', user[1], '\t', user[2], '\t', user[3], '\t', user[4])

    print()

    return None

############# TESTS #############

